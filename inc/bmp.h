#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t DWORD;

#pragma pack(1)
typedef struct BitmapFileHeader
{
    WORD  signature;
    DWORD size;
    WORD  reserved1;
    WORD  reserved2;
    DWORD offset;
}BitmapFileHeader;

typedef struct BitmapInfoHeader
{
    DWORD structSize;
    DWORD width;
    DWORD height;
    WORD  planes;
    WORD  bitsPerPixel;
    DWORD compressionType;
    DWORD imageSize;
    DWORD hResolution;
    DWORD vResolution;
    DWORD numColors;
    DWORD impColors;    
}BitmapInfoHeader;

BYTE * loadBmp(BitmapFileHeader * fHeader, BitmapInfoHeader * iHeader, char * filename);
BYTE * saveBmp(BitmapFileHeader * fHeader, BitmapInfoHeader * iHeader, char * filename);

