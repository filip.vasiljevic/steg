OBJS = bin/main.o bin/bmp.o

all: app

app: ${OBJS}
	gcc -o app ${OBJS}

bin/%.o: src/%.c inc/bmp.h
	gcc -c -I inc $< -o $@

clean: 
	rm -f app
	rm -f bin/*.o
