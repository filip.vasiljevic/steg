#include "bmp.h"

int main()
{
    BYTE * bmp;
    BitmapFileHeader fHeader;
    BitmapInfoHeader iHeader;

    bmp = loadBmp(&fHeader, &iHeader, "slika.bmp");

    printf("Sirina slike je: %d\n", iHeader.width);
    printf("Visina slike je: %d\n", iHeader.height);

    return 0;
}
