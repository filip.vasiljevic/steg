#include "bmp.h"

BYTE * loadBmp(BitmapFileHeader * fHeader, BitmapInfoHeader * iHeader, char * filename)
{
    BYTE * bmp;
    FILE * file;
    file = fopen(filename, "r");
    fread(fHeader, sizeof(BitmapFileHeader), 1, file);
    fread(iHeader, sizeof(BitmapInfoHeader), 1, file);
    bmp = malloc(fHeader->size);
    fread(bmp, fHeader->size, 1, file);

    return bmp;
}
